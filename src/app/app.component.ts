import { Component, OnInit } from '@angular/core';
import { Configuration } from './shared/interfaces/confuguration.interface';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  title = 'SmartsoftTestFrontEnd';
  imtemSelected: Configuration

  constructor() { }
  ngOnInit() { }

}
