
export interface TableDetail {
    id: number;
    name: string
    colums: Colums
}


export interface Colums {
    id: number,
    header: string,
    dataType: string,
    required: false
    format?: string,
}