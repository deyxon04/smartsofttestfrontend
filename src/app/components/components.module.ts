import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ConfigurationComponent } from './configuration/configuration.component';
import { DataComponent } from './data/data.component';
import { DataService, DropDownsModule } from '@progress/kendo-angular-dropdowns';
import { GridModule } from '@progress/kendo-angular-grid';
import { ConfigurationService } from '../services/configuration.service';
import { HttpClientModule } from "@angular/common/http";
import { IconsModule } from '@progress/kendo-angular-icons';
import { DateInputsModule } from '@progress/kendo-angular-dateinputs';
import { DialogModule } from '@progress/kendo-angular-dialog';
import { LayoutModule } from '@progress/kendo-angular-layout';
import { NotificationModule } from '@progress/kendo-angular-notification';

@NgModule({
    imports: [
        CommonModule,
        DropDownsModule,
        GridModule,
        HttpClientModule,
        IconsModule,
        DateInputsModule,
        DialogModule,
        LayoutModule,
        NotificationModule
    ],
    declarations: [ConfigurationComponent, DataComponent],
    exports: [ConfigurationComponent, DataComponent],
    providers: [ConfigurationService, DataService]
})
export class ComponentsModule { }
