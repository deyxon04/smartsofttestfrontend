import { Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { DataService } from 'src/app/services/data.service';;
import { State } from '@progress/kendo-data-query';
import { ConfigurationService } from 'src/app/services/configuration.service';
import { Configuration } from 'src/app/shared/interfaces/confuguration.interface';
import { Colums } from 'src/app/shared/interfaces/tabledetail.interface';
import { DialogCloseResult, DialogRef, DialogService } from '@progress/kendo-angular-dialog';
import { NotificationService } from '@progress/kendo-angular-notification';

@Component({
  selector: 'app-data',
  templateUrl: './data.component.html',
  styleUrls: ['./data.component.css']
})
export class DataComponent implements OnInit, OnChanges {




  @Input() itemSelected: Configuration

  columnsGrid: any[]
  infoGrid: any[]


  formGroup: FormGroup;

  private editedRowIndex: number;

  gridState: State = {
    sort: [],
    skip: 0,
    take: 10
  };

  constructor(private configuratioService: ConfigurationService, private dataService: DataService, private dialogService: DialogService, private notificationService: NotificationService) {
    this.columnsGrid = []
    this.infoGrid = []

  }

  ngOnInit() { }


  addHandler({ sender }) {
    this.closeEditor(sender);
    this.formGroup = new FormGroup({})
    this.columnsGrid.forEach((element: Colums) => {
      this.formGroup.setControl(element.header, this.createFormControl(element))
    })
    sender.addRow(this.formGroup);
  }


  editHandler({ sender, rowIndex, dataItem }) {
    this.closeEditor(sender);
    this.formGroup = new FormGroup({})
    this.columnsGrid.forEach((element: Colums) => {
      this.formGroup.setControl(element.header, this.createFormControl(element, dataItem))
    })
    this.editedRowIndex = rowIndex;
    sender.editRow(rowIndex, this.formGroup);
  }

  cancelHandler({ sender, rowIndex }) {
    this.closeEditor(sender, rowIndex);
  }

  saveHandler({ sender, rowIndex, formGroup, isNew, dataItem }) {

    for (const i in this.formGroup.controls) {
      this.formGroup.controls[i].markAsDirty();
      this.formGroup.controls[i].updateValueAndValidity();
    }
    const product = formGroup.value;

    if (this.formGroup.valid && isNew) {
      product.TableTypeId = this.itemSelected.id
      this.dataService.saveItem(product).subscribe(response => {
        this.infoGrid.unshift(response)
        sender.closeRow(rowIndex);

        this.succesMessage(isNew)


      }, err => {
        this.errorMessage()
      })
    }

    if (this.formGroup.valid && !isNew) {
      this.dataService.updateItem(dataItem['id'], this.itemSelected.id, product).subscribe(response => {
        this.infoGrid[rowIndex] = response
        sender.closeRow(rowIndex);

        this.succesMessage(true)
      }, err => {
        this.errorMessage()
      })
    }
  }

  removeHandler({ dataItem, rowIndex }) {

    const dialog: DialogRef = this.dialogService.open({
      title: 'Confirmar acción',
      content: '¿Desea eliminar el registro?',
      actions: [
        { text: 'Si', status: true },
        { text: 'No', status: false, primary: true }
      ],
      width: 450,
      height: 200,
      minWidth: 250
    });

    dialog.result.subscribe((result) => {
      if (result['status']) this.deleteItem(dataItem, rowIndex)
    });
  }

  setDate(event: Date) {
    this.formGroup.get('T1C4')
  }

  private closeEditor(grid, rowIndex = this.editedRowIndex) {
    grid.closeRow(rowIndex);
    this.editedRowIndex = undefined;
    this.formGroup = undefined;
  }


  ngOnChanges(changes: SimpleChanges) {
    const { currentValue } = changes.itemSelected
    if (currentValue) {
      this.configuratioService.getTableDetail(currentValue['id']).subscribe((response) => {
        this.columnsGrid = response['colums']
        this.getTableInfo()
      }, err => {
        this.errorMessage()
      })
    }
  }


  private deleteItem(dataItem, rowIndex) {
    this.dataService.deleteItem({ id: dataItem.id, tableTypeId: this.itemSelected.id }).subscribe(response => {
      this.infoGrid.splice(rowIndex, 1)
      this.succesMessage(true)
    }, err => {
      this.errorMessage()
    })

  }

  getTableInfo() {
    this.dataService.getItems(this.itemSelected.id).subscribe(response => {
      this.infoGrid = response
    }, err => {
      this.errorMessage()
    })
  }

  

  private createFormControl(controlData: Colums, dataItem?) {


    const control: FormControl = new FormControl(dataItem ? dataItem[controlData.header] : null)
    const validators: any[] = []
    if (controlData.required) validators.push(Validators.required)
    if (controlData.dataType == 'Int') validators.push(Validators.pattern('^[0-9]{1,11}'))
    control.setValidators(validators)

    return control
  }


  private succesMessage(way) {
    this.notificationService.show({
      content: way ? 'Item modificado correctamente.' : 'Item creado correctamente.',
      cssClass: 'button-notification',
      hideAfter: 600,
      animation: { type: 'slide', duration: 200 },
      position: { horizontal: 'right', vertical: 'top' },
      type: { style: 'success', icon: true },
      closable: true
    });
  }

  private errorMessage() {
    this.notificationService.show({
      content: 'Ha ocurrido un error',
      cssClass: 'button-notification',
      hideAfter: 600,
      animation: { type: 'slide', duration: 200 },
      position: { horizontal: 'right', vertical: 'top' },
      type: { style: 'error', icon: true },
      closable: true
    });
  }


}

