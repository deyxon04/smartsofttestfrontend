import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { ConfigurationService } from 'src/app/services/configuration.service';
import { Configuration } from 'src/app/shared/interfaces/confuguration.interface';

@Component({
  selector: 'app-configuration',
  templateUrl: './configuration.component.html',
  styleUrls: ['./configuration.component.css']
})
export class ConfigurationComponent implements OnInit {


  listItems: Array<Configuration>

  @Output() dataSelected: EventEmitter<Configuration> = new EventEmitter();

  constructor(private configuratioService: ConfigurationService) {
    this.listItems = []
  }

  ngOnInit() {
    this.getConfiguration()
  }


  private getConfiguration() {
    this.configuratioService.getTables().subscribe((response) => {
      this.listItems = response
      this.loadData()
    }, err => {
      console.log(err);
    })
  }


  loadData(data?) {
    this.dataSelected.emit(data ? data : this.listItems[0])
  }

}
