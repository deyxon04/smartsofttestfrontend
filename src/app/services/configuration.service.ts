import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { Configuration } from '../shared/interfaces/confuguration.interface';
import { TableDetail } from '../shared/interfaces/tabledetail.interface';

@Injectable({
  providedIn: 'root'
})
export class ConfigurationService {


  private url: string

  constructor(private _http: HttpClient) {

    this.url = 'http://localhost:8400/api/configuration/'
  }





  getTables(): Observable<Configuration[]> {
    return this._http.get(this.url + 'getTables').pipe((map((resp: any) => {
      return resp
    })), catchError(((err) => {
      return throwError(err.error);
    })));
  }



  getTableDetail(id): Observable<TableDetail[]> {
    return this._http.get(this.url + `getTableDetail/${id}`).pipe((map((resp: any) => {
      return resp
    })), catchError(((err) => {
      return throwError(err.error);
    })));
  }


}
