import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { TableDetail } from '../shared/interfaces/tabledetail.interface';
import { map, catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})


export class DataService {


  private url: string

  constructor(private _http: HttpClient) {

    this.url = 'http://localhost:8400/api/tableData/'
  }




  getItems(id) {

    console.log(id);
    
    return this._http.get(this.url + `${id}`).pipe((map((resp: any) => {
      return resp
    })), catchError(((err) => {
      return throwError(err.error);
    })));
  }


  saveItem(body) {
    return this._http.post(this.url, body, {}).pipe((map((resp: any) => {
      return resp
    })), catchError(((err) => {
      return throwError(err.error);
    })));
  }



  deleteItem({ id, tableTypeId }) {
    return this._http.delete(this.url + `${id}/${tableTypeId}`).pipe((map((resp: any) => {
      return resp
    })), catchError(((err) => {
      return throwError(err.error);
    })));
  }



  updateItem(id, tableTypeId, body) {
    return this._http.put(this.url + `${id}/${tableTypeId}`, body).pipe((map((resp: any) => {
      return resp
    })), catchError(((err) => {
      return throwError(err.error);
    })));
  }






}
